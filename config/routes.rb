Rails.application.routes.draw do
  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'
  get '/', to: redirect('/api-docs')
  namespace :api do
    namespace :v1 do
      resources :foods do
        collection do
          resources :bars,      module: :foods
          resources :cafes,     module: :foods
          resources :clubs,     module: :foods
          resources :coffees,   module: :foods
          resources :fastfoods, module: :foods
        end
      end
      resources :places do
        collection do
          resources :churches,  module: :places
          resources :monuments, module: :places
          resources :museums,   module: :places
        end
      end
      resources :users
      resource :auth, only: %i[create]
      post '/auth/login', to: 'authentication#login'
      resources :categories
      resources :identities
      resource :confirmation, only: %i[create]
      resources :identities do
        post 'confirm'
      end
      get '/*a', to: 'application#not_found'
    end
  end
end
