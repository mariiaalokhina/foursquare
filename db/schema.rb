# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_01_03_143050) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "hstore"
  enable_extension "plpgsql"

  create_table "categories", force: :cascade do |t|
    t.string "name_category"
    t.string "name_type"
    t.string "url"
    t.string "img_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "favorite_places", force: :cascade do |t|
    t.bigint "user_id"
    t.index ["user_id"], name: "index_favorite_places_on_user_id"
  end

  create_table "favorite_places_foods", id: false, force: :cascade do |t|
    t.bigint "food_id", null: false
    t.bigint "favorite_place_id", null: false
    t.index ["favorite_place_id", "food_id"], name: "index_favorite_places_foods_on_favorite_place_id_and_food_id", unique: true
    t.index ["food_id", "favorite_place_id"], name: "index_favorite_places_foods_on_food_id_and_favorite_place_id", unique: true
  end

  create_table "favorite_places_places", id: false, force: :cascade do |t|
    t.bigint "place_id", null: false
    t.bigint "favorite_place_id", null: false
    t.index ["favorite_place_id", "place_id"], name: "index_favorite_places_places_on_favorite_place_id_and_place_id", unique: true
    t.index ["place_id", "favorite_place_id"], name: "index_favorite_places_places_on_place_id_and_favorite_place_id", unique: true
  end

  create_table "foods", force: :cascade do |t|
    t.string "name"
    t.string "type_name"
    t.string "address"
    t.bigint "foursquare_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "status"
    t.jsonb "details", default: {}, null: false
    t.point "coordinates"
    t.float "mark"
    t.integer "price_policy"
    t.jsonb "open_hours", default: {}, null: false
    t.index ["details"], name: "index_foods_on_details", using: :gin
    t.index ["foursquare_id"], name: "index_foods_on_foursquare_id"
  end

  create_table "foods_saved_places", id: false, force: :cascade do |t|
    t.bigint "food_id", null: false
    t.bigint "saved_place_id", null: false
    t.index ["food_id", "saved_place_id"], name: "index_foods_saved_places_on_food_id_and_saved_place_id", unique: true
    t.index ["saved_place_id", "food_id"], name: "index_foods_saved_places_on_saved_place_id_and_food_id", unique: true
  end

  create_table "foursquares", force: :cascade do |t|
    t.string "section"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "identities", force: :cascade do |t|
    t.string "username"
    t.string "email"
    t.string "password_digest"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.index ["user_id"], name: "index_identities_on_user_id"
  end

  create_table "notifications", force: :cascade do |t|
    t.string "recipient"
    t.string "action"
    t.string "notifiable"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "open_hours", force: :cascade do |t|
  end

  create_table "places", force: :cascade do |t|
    t.string "name"
    t.string "type_name"
    t.string "address"
    t.bigint "foursquare_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "status"
    t.jsonb "details", default: {}, null: false
    t.point "coordinates"
    t.float "mark"
    t.integer "price_policy"
    t.jsonb "open_hours", default: {}, null: false
    t.index ["details"], name: "index_places_on_details", using: :gin
    t.index ["foursquare_id"], name: "index_places_on_foursquare_id"
  end

  create_table "places_saved_places", id: false, force: :cascade do |t|
    t.bigint "place_id", null: false
    t.bigint "saved_place_id", null: false
    t.index ["place_id", "saved_place_id"], name: "index_places_saved_places_on_place_id_and_saved_place_id", unique: true
    t.index ["saved_place_id", "place_id"], name: "index_places_saved_places_on_saved_place_id_and_place_id", unique: true
  end

  create_table "saved_places", force: :cascade do |t|
    t.bigint "user_id"
    t.index ["user_id"], name: "index_saved_places_on_user_id"
  end

  create_table "sessions", force: :cascade do |t|
    t.string "session_id", null: false
    t.text "data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["session_id"], name: "index_sessions_on_session_id", unique: true
    t.index ["updated_at"], name: "index_sessions_on_updated_at"
  end

  create_table "user_has_foursquares", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "foursquare_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["foursquare_id"], name: "index_user_has_foursquares_on_foursquare_id"
    t.index ["user_id"], name: "index_user_has_foursquares_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "active", default: 0
    t.integer "role_mask"
  end

end
