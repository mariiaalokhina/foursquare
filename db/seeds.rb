identity = Identity.create!(username: 'FridaKhalo1907',
                            email: 'example@placecase.org',
                            password: '123123')
identity.mark_as_confirmed!
identity.user.update(first_name: 'Example first name',
                     last_name: 'Example last name',
                     role_mask: 1)

9.times do
  first_name = Faker::Name.first_name
  last_name = Faker::Name.last_name
  email = Faker::Internet.email
  password = '123123'
  identity = Identity.create!(username: first_name,
                              email: email,
                              password: password)
  identity.mark_as_confirmed!
  identity.user.update(first_name: first_name,
                       last_name: last_name,
                       role_mask: 1)
end

Foursquare.create!(section: 'Place')
Foursquare.create!(section: 'Food')

category_list = [%w(Place Church church icons_church),
                 %w(Place Monument monument icons_monument),
                 %w(Place Museum museum icons_museum),
                 %w(Food Bar bar icons_bar),
                 %w(Food Cafe cafe icons_cafe),
                 %w(Food Club club icons_club),
                 %w(Food Coffee coffee icons_coffee),
                 %w(Food Fastfood fastfood icons_fastfood)]

category_list.each do |category, type, url, img_url|
  Category.create!(name_category: category,
                   name_type: type,
                   url: url,
                   img_url: img_url)
end

