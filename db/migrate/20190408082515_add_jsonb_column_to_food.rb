class AddJsonbColumnToFood < ActiveRecord::Migration[5.2]
  def change
    add_column :foods, :details, :jsonb, null: false, default: {}
    add_index  :foods, :details, using: :gin
  end
end
