class CreateSavedPlace < ActiveRecord::Migration[5.2]
  def change
    create_table :saved_places do |t|
      t.belongs_to :user
    end
  end
end
