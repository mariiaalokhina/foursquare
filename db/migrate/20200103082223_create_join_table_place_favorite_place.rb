class CreateJoinTablePlaceFavoritePlace < ActiveRecord::Migration[5.2]
  def change
    create_join_table :places, :favorite_places do |t|
      # t.index [:place_id, :favorite_place_id]
      # t.index [:favorite_place_id, :place_id]
    end
    create_join_table :foods, :favorite_places do |t|
      # t.index [:place_id, :favorite_place_id]
      # t.index [:favorite_place_id, :place_id]
    end
    change_table :favorite_places do |t|
      t.remove :place
    end
  end
end
