class AddIndexPlaceSavedPlace < ActiveRecord::Migration[5.2]
  def change
    add_index :foods_saved_places, [:food_id, :saved_place_id], unique: true
    add_index :foods_saved_places, [:saved_place_id, :food_id], unique: true
    add_index :places_saved_places, [:place_id, :saved_place_id], unique: true
    add_index :places_saved_places, [:saved_place_id, :place_id], unique: true
  end
end
