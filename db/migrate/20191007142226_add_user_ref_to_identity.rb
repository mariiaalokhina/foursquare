class AddUserRefToIdentity < ActiveRecord::Migration[5.2]
  def change
    add_reference :identities, :user, index: true
  end
end
