class AddCoordinatesToFood < ActiveRecord::Migration[5.2]
  def change
    add_column :foods, :coordinates, :point
  end
end
