class AddJsonbColumnToPlace < ActiveRecord::Migration[5.2]
  def change
    add_column :places, :details, :jsonb, null: false, default: {}
    add_index  :places, :details, using: :gin
  end
end
