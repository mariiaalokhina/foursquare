class AddStatusToFood < ActiveRecord::Migration[5.2]
  def change
    add_column :foods, :status, :string
  end
end
