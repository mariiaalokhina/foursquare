class AddOmniauthToUsers < ActiveRecord::Migration[5.2]
  change_table :users, bulk: true do |t|
    t.string :provider, null: false
    t.string :uid
  end
end
