class AddPlacesToFavoritePlace < ActiveRecord::Migration[5.2]
  def change
    add_column :favorite_places, :place, :jsonb, null: false, default: {}
  end
end
