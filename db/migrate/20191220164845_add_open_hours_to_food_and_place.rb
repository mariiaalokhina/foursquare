class AddOpenHoursToFoodAndPlace < ActiveRecord::Migration[5.2]
  def change
    add_column :foods, :open_hours, :jsonb, null: false, default: {}
    add_column :places, :open_hours, :jsonb, null: false, default: {}
  end
end
