class CreateFoods < ActiveRecord::Migration[5.2]
  def change
    create_table :foods do |t|
      t.string :name
      t.string :type_name
      t.string :address
      t.belongs_to :foursquare, index: true
      t.timestamps
    end
  end
end
