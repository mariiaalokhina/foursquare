class CreateNotifications < ActiveRecord::Migration[5.2]
  def change
    create_table :notifications do |t|
      t.string :recipient
      t.string :action
      t.string :notifiable

      t.timestamps
    end
  end
end
