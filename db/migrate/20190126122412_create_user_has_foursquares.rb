class CreateUserHasFoursquares < ActiveRecord::Migration[5.2]
  def change
    create_table :user_has_foursquares do |t|
      t.belongs_to :user, index: true
      t.belongs_to :foursquare, index: true
      t.timestamps
    end
  end
end
