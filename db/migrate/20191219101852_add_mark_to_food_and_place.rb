class AddMarkToFoodAndPlace < ActiveRecord::Migration[5.2]
  def change
    add_column :places, :mark,:float
    add_column :foods,  :mark,:float
  end
end
