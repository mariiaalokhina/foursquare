class AddStatusToPlace < ActiveRecord::Migration[5.2]
  def change
    add_column :places, :status, :string
  end
end
