class AddPricePolicyToFoodAndPlace < ActiveRecord::Migration[5.2]
  def change
    add_column :places, :price_policy, :integer
    add_column :foods,  :price_policy, :integer
  end
end
