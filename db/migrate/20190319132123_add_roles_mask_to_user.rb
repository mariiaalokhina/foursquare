class AddRolesMaskToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :role_mask, :integer
  end
end
