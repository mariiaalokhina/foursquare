class AddConfirmationToIdentity < ActiveRecord::Migration[5.2]
  def change
    add_column :identities, :confirmation_token,   :string
    add_column :identities, :confirmed_at,         :datetime
    add_column :identities, :confirmation_sent_at, :datetime

  end
end
