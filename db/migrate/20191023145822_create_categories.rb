class CreateCategories < ActiveRecord::Migration[5.2]
  def change
    create_table :categories do |t|
      t.string :name_category
      t.string :name_type
      t.string :url
      t.string :img_url

      t.timestamps
    end
  end
end
