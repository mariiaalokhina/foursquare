class CreateFoursquares < ActiveRecord::Migration[5.2]
  def change
    create_table :foursquares do |t|
      t.string :section

      t.timestamps
    end
  end
end
