class RemoveUnnecessaryFieldsFromUser < ActiveRecord::Migration[5.2]
  def change
    remove_index :users, name: :index_users_on_confirmation_token
    remove_index :users, name: :index_users_on_reset_password_token

    change_table :users do |t|
      t.remove :email
      t.remove :provider
      t.remove :uid
      t.remove :confirmation_token
      t.remove :confirmed_at
      t.remove :confirmation_sent_at
      t.remove :unconfirmed_email
      t.remove :encrypted_password
      t.remove :reset_password_token
      t.remove :reset_password_sent_at
      t.remove :remember_created_at
    end
  end
end
