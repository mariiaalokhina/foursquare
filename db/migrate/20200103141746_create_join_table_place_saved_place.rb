class CreateJoinTablePlaceSavedPlace < ActiveRecord::Migration[5.2]
  def change
    create_join_table :places, :saved_places do |t|
      # t.index [:place_id, :saved_place_id]
      # t.index [:saved_place_id, :place_id]
    end
    create_join_table :foods, :saved_places do |t|
      # t.index [:place_id, :saved_place_id]
      # t.index [:saved_place_id, :place_id]
    end
  end
end
