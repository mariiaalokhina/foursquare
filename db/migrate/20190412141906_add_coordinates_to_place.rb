class AddCoordinatesToPlace < ActiveRecord::Migration[5.2]
  def change
    add_column :places, :coordinates, :point
  end
end
