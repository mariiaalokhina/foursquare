class AddIndexPlaceFavoritePlace < ActiveRecord::Migration[5.2]
  def change
    add_index :favorite_places_foods, [:food_id, :favorite_place_id], unique: true
    add_index :favorite_places_foods, [:favorite_place_id, :food_id], unique: true
    add_index :favorite_places_places, [:place_id, :favorite_place_id], unique: true
    add_index :favorite_places_places, [:favorite_place_id, :place_id], unique: true
  end
end
