source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.5.1'
gem 'active_model_serializers'
gem 'activerecord-session_store'
gem 'bcrypt'
gem 'bootsnap', '>= 1.1.0', require: false
gem 'cancancan', '~> 2.0'
gem 'devise'
gem 'elasticsearch-model', github: 'elastic/elasticsearch-rails', branch: '5.x'
gem 'elasticsearch-rails', github: 'elastic/elasticsearch-rails', branch: '5.x'
gem 'foursquare_catalog', github: 'MariiaAlehina/foursquare_catalog'
gem 'geocoder'
gem 'jwt'
gem 'nokogiri', '1.10.1'
gem 'omniauth-facebook'
gem 'omniauth-google-oauth2'
gem 'pg', '>= 0.18', '< 2.0'
gem 'pry'
gem 'puma', '~> 3.11'
gem 'rails', '~> 5.2.2'
gem 'redis'
gem 'rswag-api'
gem 'rswag-ui'
gem 'websocket-rails'
group :development, :test do
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'capybara', '~> 3.5'
  gem 'factory_bot_rails'
  gem 'faker', '1.6.5'
  gem 'rspec-rails', '~> 3.8'
  gem 'rubocop'
  gem 'shoulda-matchers', git: 'https://github.com/thoughtbot/shoulda-matchers.git', branch: 'rails-5'
  gem 'simplecov'
  gem 'simplecov-console'
  gem 'smart_rspec'
end

group :test do
  gem 'database_cleaner', '~> 1.5'
  gem 'rails-controller-testing'
  gem 'rswag-specs'
end

group :development do
  gem 'dotenv'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

gem 'rack-cors', require: 'rack/cors'
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
