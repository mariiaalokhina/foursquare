# require 'swagger_helper'
#
# describe 'Foursquare API' do
#   path '/users' do
#     post 'Creates a user' do
#       tags 'Users'
#       consumes 'application/json', 'application/xml'
#       parameter name: :users, in: :body, schema: {
#         type: :object,
#         properties: {
#           first_name: { type: :string },
#           last_name: { type: :string },
#           email: { type: :string },
#           active: { type: :integer }
#         },
#         required: %w[first_name last_name email active]
#       }
#
#       response '201', 'user created' do
#         let(:user) do
#           { first_name: 'Raf', last_name: 'Oskar',
#             email: 'oskar@raf.com', active: 0 }
#         end
#         run_test!
#       end
#
#       response '422', 'invalid request' do
#         let(:user) { { first_name: 'foo' } }
#         run_test!
#       end
#     end
#   end
#
#   path '/users/{id}' do
#     get 'Retrieves a user' do
#       tags 'Users'
#       produces 'application/json', 'application/xml'
#       parameter first_name: :id, in: :path, type: :string
#
#       response '200', 'first_name found' do
#         schema type: :object,
#                properties: {
#                  id: { type: :integer },
#                  first_name: { type: :string },
#                  last_name: { type: :string },
#                  email: { type: :string },
#                  active: { type: :integer }
#                },
#                required: %w[id first_name last_name email active]
#
#         let(:id) do
#           User.create(first_name: 'Raf', last_name: 'Oskar',
#                       email: 'oskar@raf.com', active: 0).id
#         end
#         run_test!
#       end
#
#       response '404', 'user not found' do
#         let(:id) { 'invalid' }
#         run_test!
#       end
#     end
#   end
# end
