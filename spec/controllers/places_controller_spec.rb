require 'rails_helper'

RSpec.describe PlacesController, type: :controller do
  let(:valid_attributes) do
    skip('Add a hash of attributes valid for your model')
  end

  let(:invalid_attributes) do
    skip('Add a hash of attributes invalid for your model')
  end

  let(:valid_session) { {} }

  describe 'GET #index' do
    it 'returns a success response' do
      place = Place.create! valid_attributes
      get :index, params: {}, session: valid_session
      expect(response).to be_successful
    end
  end

  describe 'GET #show' do
    it 'returns a success response' do
      place = Place.create! valid_attributes
      get :show, params: { id: place.to_param }, session: valid_session
      expect(response).to be_successful
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new Place' do
        expect do
          post :create, params: { places: valid_attributes }, session: valid_session
        end.to change(Place, :count).by(1)
      end

      it 'renders a JSON response with the new places' do
        post :create, params: { places: valid_attributes }, session: valid_session
        expect(response).to have_http_status(:created)
        expect(response.content_type).to eq('application/json')
        expect(response.location).to eq(place_url(Place.last))
      end
    end

    context 'with invalid params' do
      it 'renders a JSON response with errors for the new places' do
        post :create, params: { places: invalid_attributes }, session: valid_session
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq('application/json')
      end
    end
  end

  describe 'PUT #update' do
    context 'with valid params' do
      let(:new_attributes) do
        skip('Add a hash of attributes valid for your model')
      end

      it 'updates the requested places' do
        place = Place.create! valid_attributes
        put :update, params: { id: place.to_param, places: new_attributes }, session: valid_session
        place.reload
        skip('Add assertions for updated state')
      end

      it 'renders a JSON response with the places' do
        place = Place.create! valid_attributes

        put :update, params: { id: place.to_param, places: valid_attributes }, session: valid_session
        expect(response).to have_http_status(:ok)
        expect(response.content_type).to eq('application/json')
      end
    end

    context 'with invalid params' do
      it 'renders a JSON response with errors for the places' do
        place = Place.create! valid_attributes

        put :update, params: { id: place.to_param, places: invalid_attributes }, session: valid_session
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq('application/json')
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the requested places' do
      place = Place.create! valid_attributes
      expect do
        delete :destroy, params: { id: place.to_param }, session: valid_session
      end.to change(Place, :count).by(-1)
    end
  end
end
