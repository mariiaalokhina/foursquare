FactoryBot.define do
  factory :foursquare do
    section { Faker::Name.first_name }
  end
end
