FactoryBot.define do
  factory :category do
    name_category { 'Place' }
    name_type     { Faker::Company.name }
    url           { Faker::Internet.url }
    img_url       { Faker::Internet.url }
  end
end
