FactoryBot.define do
  factory :identity do
    username     { Faker::Name.first_name }
    email        { Faker::Internet.email }
    password     { '123123' }
  end
end
