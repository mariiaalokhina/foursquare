FactoryBot.define do
  factory :user_has_foursquare do
    user
    foursquare
  end
end
