FactoryBot.define do
  factory :user do
    first_name  { Faker::Name.first_name }
    last_name   { Faker::Name.last_name }
    active      { 1 }
    role_mask   { 2 }
  end
end
