FactoryBot.define do
  factory :notification do
    recipient       { Faker::Space.planet }
    action           { 'create' }
    notifiable       { Faker::Space.planet }
  end
end
