FactoryBot.define do
  factory :monument do
    name          { Faker::Company.name }
    address       { Faker::Address.street_name }
    status        { 'inactive' }
    details       { Faker::Address.street_name }
    mark          { '3.2' }
    price_policy  { 1 }
    open_hours do
      { 'Mon' => { 'open' => '10:00', 'close' => '19:00' },
        'Tue' => { 'open' => '10:00', 'close' => '19:00' },
        'Wed' => { 'open' => '10:00', 'close' => '19:00' },
        'Thu' => { 'open' => '10:00', 'close' => '19:00' },
        'Fri' => { 'open' => '10:00', 'close' => '19:00' },
        'Sat' => { 'open' => '10:00', 'close' => '19:00' },
        'Sun' => { 'open' => '10:00', 'close' => '19:00' } }
    end

    association :foursquare, factory: :foursquare, strategy: :create
  end
end
