require 'rails_helper'

RSpec.describe User, type: :model do
  it { should define_enum_for(:active) }

  it { should have_many(:user_has_foursquares).dependent(:destroy) }
  it { should have_many(:foursquares).through(:user_has_foursquares) }

  let(:user) { create(:user) }

  describe 'user roles' do
    it 'user role should be admin' do
      user.role_mask = 1

      expect(user.is?(:admin)).to eq true
    end

    it 'user role should be banned' do
      user.role_mask = 2

      expect(user.is?(:banned)).to eq true
    end

    it '#roles' do
      user.role_mask = 2

      user.roles = 'admin'
      expect(user.is?(:admin)).to eq true
    end
  end

  describe 'create places' do
    it '#create_place' do
      place = user.create_place('Test', 'Church', 'test')

      expect(place.present?).to eq true
    end

    it '#create_food' do
      food = user.create_food('Test', 'Bar', 'test')

      expect(food.present?).to eq true
    end

    it '#change_status' do
      bar = create(:bar)
      status = 'active'
      user.change_status(bar, status)

      expect(bar.status).to eq status
    end
  end

  describe 'Searching for a user', elasticsearch: true do
    before do
      User.__elasticsearch__.create_index! index: User.index_name
      User.create!
      User.import
      sleep 1
    end

    after do
      User.__elasticsearch__.client.indices.delete index: User.index_name
    end
  end

  describe "favorite and save user's places" do
    it '#make_save food object' do
      bar = build(:bar)
      user.make_save(bar)

      expect(user.saved_place.foods.include?(bar)).to eq true
    end

    it '#make_save place object' do
      church = build(:church)
      user.make_save(church)

      expect(user.saved_place.places.include?(church)).to eq true
    end

    it "raise error if attribute doen't Place or Food make_save" do
      error_class = build(:category)

      expect { user.make_save(error_class) }.to raise_error(RuntimeError)
    end

    it '#make_favorite food object' do
      bar = build(:bar)
      user.make_favorite(bar)

      expect(user.favorite_place.foods.include?(bar)).to eq true
    end

    it '#make_favorite place object' do
      church = build(:church)
      user.make_favorite(church)

      expect(user.favorite_place.places.include?(church)).to eq true
    end

    it "raise error if attribute doen't Place or Food make_favorite" do
      error_class = build(:category)

      expect { user.make_favorite(error_class) }.to raise_error(RuntimeError)
    end
  end
end
