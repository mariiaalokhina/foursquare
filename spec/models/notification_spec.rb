require 'rails_helper'

RSpec.describe Notification, type: :model do
  it { should validate_length_of(:recipient).is_at_least(2) }
  it { should validate_length_of(:action).is_at_least(2) }
  it { should validate_uniqueness_of(:notifiable) }
end
