require 'rails_helper'

RSpec.describe Place, type: :model do
  it { should validate_length_of(:address).is_at_least(5) }

  it { should belong_to(:foursquare) }

  let(:place) { build(:place) }

  describe '#notifications' do
    it 'fires create_notifications method as after_create method callbacks' do
      expect(place).to receive(:create_notifications)
      place.run_callbacks(:create)
    end

    it 'fires destroy_notification method as after_destroy method callbacks' do
      expect(place).to receive(:destroy_notification)
      place.run_callbacks(:destroy)
    end
  end

  describe 'filter open now' do
    it '.open_now' do
      open_time = { 'Mon' => { 'open' => Time.zone.now, 'close' => Time.zone.now + 3.hours },
                    'Tue' => { 'open' => Time.zone.now, 'close' => Time.zone.now + 3.hours },
                    'Wed' => { 'open' => Time.zone.now, 'close' => Time.zone.now + 3.hours },
                    'Thu' => { 'open' => Time.zone.now, 'close' => Time.zone.now + 3.hours },
                    'Fri' => { 'open' => Time.zone.now, 'close' => Time.zone.now + 3.hours },
                    'Sat' => { 'open' => Time.zone.now, 'close' => Time.zone.now + 3.hours },
                    'Sun' => { 'open' => Time.zone.now, 'close' => Time.zone.now + 3.hours } }

      open_place = create(:place, open_hours: open_time)

      expect(Place.open_now(Time.zone.now).include?(open_place)).to eq true
    end
  end
end
