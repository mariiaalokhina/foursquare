require 'rails_helper'

RSpec.describe Identity, type: :model do
  it { should belong_to(:user) }

  let(:identity) { create(:identity) }

  describe '#create_identity' do
    it 'create identity' do
      identity = create(:identity)

      expect(identity.present?).to eq true
      expect(identity.valid?).to eq true
      expect(identity.user.present?).to eq true
    end
  end

  describe 'confirmation token' do
    it 'confirmation_token_valid should be valid' do
      identity = build(:identity, confirmation_sent_at: Time.now.utc)

      expect(identity.confirmation_token_valid?).to eq true
    end

    it 'confirmation_token_valid should be invalid' do
      identity = build(:identity, confirmation_sent_at: Time.now.utc - 31.days)

      expect(identity.confirmation_token_valid?).to eq false
    end

    it '#mark_as_confirmed!' do
      identity = create(:identity)

      identity.mark_as_confirmed!
      expect(identity.confirmation_token.blank?).to eq true
      expect(identity.confirmed_at.present?).to eq true
    end
  end
end
