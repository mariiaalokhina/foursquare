require 'rails_helper'

RSpec.describe Category, type: :model do
  describe 'validations' do
    it { should validate_presence_of(:name_category) }
    it { should validate_presence_of(:name_type) }
  end

  let(:category) { create(:category) }

  describe '#create_category' do
    it 'create category' do
      category = create(:category)

      expect(category.present?).to eq true
      expect(category.valid?).to eq true
    end
  end
end
