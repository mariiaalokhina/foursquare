require 'rails_helper'

RSpec.describe Club, type: :model do
  describe 'associations' do
    it 'should inherit behavior from Food' do
      expect(Club.superclass).to eq(Food)
    end
    it { should belong_to(:foursquare) }
  end

  describe 'validations' do
    it { should validate_presence_of(:status) }
    it { should validate_presence_of(:address) }
    it { should validate_length_of(:address).is_at_least(5) }
  end

  it 'create club' do
    club = create(:club)

    expect(club.type_name).to eq 'Club'
    expect(club.valid?).to    eq true
  end
end
