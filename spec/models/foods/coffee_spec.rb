require 'rails_helper'

RSpec.describe Coffee, type: :model do
  describe 'associations' do
    it 'should inherit behavior from Food' do
      expect(Coffee.superclass).to eq(Food)
    end
    it { should belong_to(:foursquare) }
  end

  describe 'validations' do
    it { should validate_presence_of(:status) }
    it { should validate_presence_of(:address) }
    it { should validate_length_of(:address).is_at_least(5) }
  end

  it 'create coffee' do
    coffee = create(:coffee)

    expect(coffee.type_name).to eq 'Coffee'
    expect(coffee.valid?).to    eq true
  end
end
