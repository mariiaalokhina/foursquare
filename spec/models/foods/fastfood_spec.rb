require 'rails_helper'

RSpec.describe Fastfood, type: :model do
  describe 'associations' do
    it 'should inherit behavior from Food' do
      expect(Fastfood.superclass).to eq(Food)
    end
    it { should belong_to(:foursquare) }
  end

  describe 'validations' do
    it { should validate_presence_of(:status) }
    it { should validate_presence_of(:address) }
    it { should validate_length_of(:address).is_at_least(5) }
  end

  it 'create fastfood' do
    fastfood = create(:fastfood)

    expect(fastfood.type_name).to eq 'Fastfood'
    expect(fastfood.valid?).to    eq true
  end
end
