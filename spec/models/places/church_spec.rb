require 'rails_helper'

RSpec.describe Church, type: :model do
  describe 'associations' do
    it 'should inherit behavior from Place' do
      expect(Church.superclass).to eq(Place)
    end
    it { should belong_to(:foursquare) }
  end

  describe 'validations' do
    it { should validate_presence_of(:status) }
    it { should validate_presence_of(:address) }
    it { should validate_length_of(:address).is_at_least(5) }
  end

  it 'create church' do
    church = create(:church)

    expect(church.type_name).to eq 'Church'
    expect(church.valid?).to    eq true
  end
end
