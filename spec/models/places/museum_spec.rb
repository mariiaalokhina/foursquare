require 'rails_helper'

RSpec.describe Museum, type: :model do
  describe 'associations' do
    it 'should inherit behavior from Place' do
      expect(Museum.superclass).to eq(Place)
    end
    it { should belong_to(:foursquare) }
  end

  describe 'validations' do
    it { should validate_presence_of(:status) }
    it { should validate_presence_of(:address) }
    it { should validate_length_of(:address).is_at_least(5) }
  end

  it 'create museum' do
    museum = create(:museum)

    expect(museum.type_name).to eq 'Museum'
    expect(museum.valid?).to    eq true
  end
end
