require 'rails_helper'

RSpec.describe Monument, type: :model do
  describe 'associations' do
    it 'should inherit behavior from Place' do
      expect(Monument.superclass).to eq(Place)
    end
    it { should belong_to(:foursquare) }
  end

  describe 'validations' do
    it { should validate_presence_of(:status) }
    it { should validate_presence_of(:address) }
    it { should validate_length_of(:address).is_at_least(5) }
  end

  it 'create monument' do
    monument = create(:monument)

    expect(monument.type_name).to eq 'Monument'
    expect(monument.valid?).to    eq true
  end
end
