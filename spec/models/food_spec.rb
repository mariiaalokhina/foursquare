require 'rails_helper'

RSpec.describe Food, type: :model do
  it { should validate_length_of(:address).is_at_least(5) }

  it { should belong_to(:foursquare) }

  let(:food) { build(:food) }

  describe '#notifications' do
    it 'fires create_notifications method as after_create method callbacks' do
      expect(food).to receive(:create_notifications)
      food.run_callbacks(:create)
    end

    it 'fires destroy_notification method as after_destroy method callbacks' do
      expect(food).to receive(:destroy_notification)
      food.run_callbacks(:destroy)
    end
  end

  describe 'filter open now' do
    it '.open_now' do
      open_time = { 'Mon' => { 'open' => Time.zone.now, 'close' => Time.zone.now + 3.hours },
                    'Tue' => { 'open' => Time.zone.now, 'close' => Time.zone.now + 3.hours },
                    'Wed' => { 'open' => Time.zone.now, 'close' => Time.zone.now + 3.hours },
                    'Thu' => { 'open' => Time.zone.now, 'close' => Time.zone.now + 3.hours },
                    'Fri' => { 'open' => Time.zone.now, 'close' => Time.zone.now + 3.hours },
                    'Sat' => { 'open' => Time.zone.now, 'close' => Time.zone.now + 3.hours },
                    'Sun' => { 'open' => Time.zone.now, 'close' => Time.zone.now + 3.hours } }

      open_food = create(:food, open_hours: open_time)

      expect(Food.open_now(Time.zone.now).include?(open_food)).to eq true
    end
  end

  describe 'notification' do
    it '#create_notifications' do
      expect(food).to receive(:create_notifications)
      food.save
    end

    it '#destroy_notification' do
      expect(food).to receive(:destroy_notification)
      food.destroy
    end
  end
end
