# Foursquare
> Placename Recommendation System API

### Dependencies
 - PostgreSQL
 - Ruby 2.5.1

### Getting started
  1. Clone this repository `git clone https://gitlab.com/mariiaalokhina/foursquare.git <FolderName>`
  2. Enter to project `cd <FolderName>`
  3. Run `bundle install`
  4. Run `rake db:create`
  5. Run `rake db:migrate`
  6. Run `rake db:seed`
  7. Run `rails s`

### Before push
  1. run `rubocop`
  2. run `rspec`


**Main rake tasks**
  - `rake db:bar` - Populating the database with bar data
  - `rake db:cafe` - Populating the database with cafe data
  - `rake db:club` - Populating the database with club data
  - `rake db:coffee` - Populating the database with coffee data
  - `rake db:fastfood` - Populating the database with fastfood data
  - `rake db:church` - Populating the database with church data
  - `rake db:monument` - Populating the database with monument data
  - `rake db:museum` - Populating the database with museum data
