class UserSerializer < ActiveModel::Serializer
  attributes :first_name, :last_name, :active, :role_mask, :favorite, :saved_place

  def favorite
    object.favorite_place.foods + object.favorite_place.places unless object.favorite_place.blank?
  end

  def saved_place
    object.saved_place.foods + object.saved_place.places unless object.saved_place.blank?
  end
end
