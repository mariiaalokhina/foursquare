module Api::V1
  class PlacesController < ApplicationController
    before_action :set_place, only: %i[show update destroy]

    def index
      @places = Place.where(nil)
      @places = @places.price_policy(params[:price_policy]) if params[:price_policy].present?
      @places = @places.open_now(params[:open_hours]) if params[:open_hours].present?

      @sort_places = @places.sort_by(&:mark).reverse

      render json: @sort_places
    end

    def show
      render json: @place
    end

    def create
      @place = Place.new(place_params)

      if @place.save
        render json: @place, status: :created, location: @place
      else
        render json: @place.errors, status: :unprocessable_entity
      end
    end

    def update
      if @place.update(place_params)
        render json: @place
      else
        render json: @place.errors, status: :unprocessable_entity
      end
    end

    def destroy
      @place.destroy
    end

    private

    def set_place
      @place = Place.find(params[:id])
    end

    def place_params
      params.require(:place).permit(:name, :type_name, :foursquare_id, :details, :coordinates,
                                    address: %i[street_address city house])
    end
  end
end
