module Api::V1
  class FoodsController < ApplicationController
    before_action :set_food, only: %i[show update destroy]

    def index
      @foods = Food.where(nil)
      @foods = @foods.price_policy(params[:price_policy]) if params[:price_policy].present?
      @foods = @foods.open_now(params[:open_hours]) if params[:open_hours].present?

      @sort_foods = @foods.sort_by(&:mark).reverse

      render json: @sort_foods
    end

    def show
      render json: @food
    end

    def create
      @food = Food.new(food_params)

      if @food.save
        render json: @food, status: :created, location: @food
      else
        render json: @food.errors, status: :unprocessable_entity
      end
    end

    def update
      if @food.update(food_params)
        render json: @food
      else
        render json: @food.errors, status: :unprocessable_entity
      end
    end

    def destroy
      @food.destroy
    end

    private

    def set_food
      @food = Food.find(params[:id])
    end

    def food_params
      params.require(:food).permit(:name, :type_name, :foursquare_id, :details, :coordinates, :price_policy,
                                   address: %i[street_address city house])
    end
  end
end
