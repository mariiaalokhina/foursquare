module Api::V1
  class AuthenticationController < ApplicationController
    before_action :authorize_request, except: :login

    def login
      @identity = Identity.find_by('email = ? OR username = ?', login_params[:email], login_params[:username])
      if @identity&.authenticate(login_params[:password])
        if @identity.confirmed_at?
          token = JsonWebToken.encode(identity_id: @identity.id)
          time = Time.zone.now + 24.hours.to_i
          render json: { token: token, exp: time.strftime('%m-%d-%Y %H:%M'),
                         username: @identity.username, userid: @identity.user.id }, status: :ok
        else
          render json: { error: 'Email not verified' }, status: :unauthorized
        end
      else
        render json: { error: 'Invalid Login or password' }, status: :unauthorized
      end
    end

    private

    def login_params
      params.permit(:email, :username, :password)
    end
  end
end
