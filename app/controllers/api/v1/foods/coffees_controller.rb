module Api::V1::Foods
  class CoffeesController < ApplicationController
    def index
      @coffes = Food.coffee.where(nil)
      @coffes = @coffes.price_policy(params[:price_policy]) if params[:price_policy].present?
      @coffes = @coffes.open_now(params[:open_hours]) if params[:open_hours].present?

      @sort_coffee = @coffes.sort_by(&:mark).reverse

      render json: @sort_coffee
    end
  end
end
