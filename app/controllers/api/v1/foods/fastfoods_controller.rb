module Api::V1::Foods
  class FastfoodsController < ApplicationController
    def index
      @fastfoods = Food.fastfood.where(nil)
      @fastfoods = @fastfoods.price_policy(params[:price_policy]) if params[:price_policy].present?
      @fastfoods = @fastfoods.open_now(params[:open_hours]) if params[:open_hours].present?

      @sort_fastfood = @fastfoods.sort_by(&:mark).reverse

      render json: @sort_fastfood
    end
  end
end
