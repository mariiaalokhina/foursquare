module Api::V1::Foods
  class BarsController < ApplicationController
    def index
      @bars = Food.bar.where(nil)
      @bars = @bars.price_policy(params[:price_policy]) if params[:price_policy].present?
      @bars = @bars.open_now(params[:open_hours]) if params[:open_hours].present?

      @sort_bars = @bars.sort_by(&:mark).reverse

      render json: @sort_bars
    end
  end
end
