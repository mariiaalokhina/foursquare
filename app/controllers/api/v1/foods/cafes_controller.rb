module Api::V1::Foods
  class CafesController < ApplicationController
    def index
      @cafe = Food.cafe.where(nil)
      @cafe = @cafe.price_policy(params[:price_policy]) if params[:price_policy].present?
      @cafe = @cafe.open_now(params[:open_hours]) if params[:open_hours].present?

      @sort_cafe = @cafe.sort_by(&:mark).reverse

      render json: @sort_cafe
    end
  end
end
