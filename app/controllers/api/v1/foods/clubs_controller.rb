module Api::V1::Foods
  class ClubsController < ApplicationController
    def index
      @clubs = Food.club.where(nil)
      @clubs = @clubs.price_policy(params[:price_policy]) if params[:price_policy].present?
      @clubs = @clubs.open_now(params[:open_hours]) if params[:open_hours].present?

      @sort_clubs = @clubs.sort_by(&:mark).reverse

      render json: @sort_clubs
    end
  end
end
