module Api::V1
  class IdentitiesController < ApplicationController
    # before_action :authorize_request, except: :create
    # before_action :find_identity, except: %i[create index]
    before_action :set_identity, only: %i[show update destroy]

    def index
      @identities = Identity.all
      render json: @identities, status: :ok
    end

    def show
      render json: @identity, status: :ok
    end

    def create
      @identity = Identity.new(identity_params)
      if @identity.save
        ExampleMailer.example_email(@identity).deliver_now
        render json: @identity, status: :created
      else
        render json: { errors: @identity.errors.full_messages },
               status: :unprocessable_entity
      end
    end

    def confirm
      @identity = Identity.find(params[:identity_id])

      if @identity.present? && @identity.confirmation_token_valid?
        @identity.mark_as_confirmed!
        render json: { status: 'Account confirmed successfully' }, status: :ok
      else
        render json: { status: 'Invalid token' }, status: :not_found
      end
    end

    private

    def set_identity
      @identity = Identity.find(params[:id])
    end

    def find_identity
      @identity = Identity.find_by!(username: params[:_username])
    rescue ActiveRecord::RecordNotFound
      render json: { errors: 'Identity not found' }, status: :not_found
    end

    def identity_params
      params.permit(
        :name, :username, :email, :password, :password_confirmation
      )
    end
  end
end
