module Api::V1::Places
  class ChurchesController < ApplicationController
    def index
      @churches = Place.church.where(nil)
      @churches = @churches.price_policy(params[:price_policy]) if params[:price_policy].present?
      @churches = @churches.open_now(params[:open_hours]) if params[:open_hours].present?

      @sort_church = @churches.sort_by(&:mark).reverse

      render json: @sort_church
    end
  end
end
