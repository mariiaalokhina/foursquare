module Api::V1::Places
  class MuseumsController < ApplicationController
    def index
      @museums = Place.museum.where(nil)
      @museums = @museums.price_policy(params[:price_policy]) if params[:price_policy].present?
      @museums = @museums.open_now(params[:open_hours]) if params[:open_hours].present?

      @sort_museum = @museums.sort_by(&:mark).reverse

      render json: @sort_museum
    end
  end
end
