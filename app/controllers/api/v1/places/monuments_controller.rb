module Api::V1::Places
  class MonumentsController < ApplicationController
    def index
      @monuments = Place.monument.where(nil)
      @monuments = @monuments.price_policy(params[:price_policy]) if params[:price_policy].present?
      @monuments = @monuments.open_now(params[:open_hours]) if params[:open_hours].present?

      @sort_monument = @monuments.sort_by(&:mark).reverse

      render json: @sort_monument
    end
  end
end
