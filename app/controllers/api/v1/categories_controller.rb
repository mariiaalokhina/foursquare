module Api::V1
  class CategoriesController < ApplicationController
    def index
      @categories = Category.all.group_by(&:name_category)

      render json: @categories
    end
  end
end
