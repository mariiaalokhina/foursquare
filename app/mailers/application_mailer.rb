class ApplicationMailer < ActionMailer::Base
  default from: 'maria.alyokhina@gmail.com'
  layout 'mailer'
end

