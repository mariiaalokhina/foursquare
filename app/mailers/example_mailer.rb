class ExampleMailer < ApplicationMailer
  def example_email(identity)
    @identity = identity
    id = @identity.id.to_s
    @url = "http://localhost:3000/api/v1/identities/"+id+"/confirm"
    mail(to: @identity.email, subject: 'Activate Account in Placecase')
  end
end
