class User < ApplicationRecord
  include ActiveModel::Serialization
  include Searchable
  include Users::Association
  include Favoriteplacable

  ROLES = %i[admin banned].freeze

  enum active: { active: 0, archived: 1 }

  def roles=(roles)
    roles = [*roles].map(&:to_sym)
    self.role_mask = (roles & ROLES).map { |r| 2**ROLES.index(r) }.inject(0, :+)
  end

  def roles
    ROLES.reject do |r|
      ((role_mask.to_i || 0) & 2**ROLES.index(r)).zero?
    end
  end

  def is?(role)
    roles.include?(role)
  end
  # move to services

  def make_save(place)
    if place.is_a?(Place)
      saved_place.places << place
    elsif place.is_a?(Food)
      saved_place.foods << place
    else
      raise
    end
  end

  def make_favorite(place)
    if place.is_a?(Place)
      favorite_place.places << place
    elsif place.is_a?(Food)
      favorite_place.foods << place
    else
      raise
    end
  end

  def create_place(name, type_name, address)
    Place.create(name: name, type_name: type_name, address: address, status: 'inactive', foursquare_id: 1)
  end

  def create_food(name, type_name, address)
    Food.create(name: name, type_name: type_name, address: address, status: 'inactive', foursquare_id: 2)
  end

  def change_status(place, status)
    place.status = status
  end
end
