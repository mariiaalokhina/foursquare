class Identity < ApplicationRecord
  include Userable

  has_secure_password

  belongs_to :user
  validates :email, presence: true, uniqueness: true
  validates :email, format: { with: URI::MailTo::EMAIL_REGEXP }
  validates :username, presence: true, uniqueness: true
  validates :password,
            length: { minimum: 6 },
            if: -> { new_record? || !password.nil? }

  before_save :downcase_email
  before_create :generate_confirmation_instructions

  def downcase_email
    self.email = email.delete(' ').downcase
  end

  def generate_confirmation_instructions
    self.confirmation_token = SecureRandom.hex(10)
    self.confirmation_sent_at = Time.now.utc
  end

  def confirmation_token_valid?
    (confirmation_sent_at + 30.days) > Time.now.utc
  end

  def mark_as_confirmed!
    update(confirmation_token: nil,
           confirmed_at: Time.now.utc)
  end
end
