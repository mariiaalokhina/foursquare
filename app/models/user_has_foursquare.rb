class UserHasFoursquare < ApplicationRecord
  belongs_to :user
  belongs_to :foursquare
end
