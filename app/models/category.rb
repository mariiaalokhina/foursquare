class Category < ApplicationRecord
  validates :name_category, presence: true
  validates :name_type, presence: true
end
