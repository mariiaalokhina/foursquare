module Users::Association
  extend ActiveSupport::Concern

  included do
    has_one :identity, dependent: :destroy
    has_one :favorite_place, dependent: :destroy
    has_one :saved_place, dependent: :destroy
    has_many :foursquares, through: :user_has_foursquares
    has_many :user_has_foursquares, dependent: :destroy
  end
end
