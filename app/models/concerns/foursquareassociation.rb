module Foursquareassociation
  extend ActiveSupport::Concern

  included do
    has_many :user_has_foursquares, dependent: :destroy
    has_many :users, through: :user_has_foursquares
    has_many :places, dependent: :destroy
    has_many :foods, dependent: :destroy
  end
end
