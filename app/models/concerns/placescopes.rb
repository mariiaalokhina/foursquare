module Placescopes
  extend ActiveSupport::Concern

  included do
    scope :church, -> { where(type_name: 'Church') }
    scope :monument, -> { where(type_name: 'Monument') }
    scope :museum, -> { where(type_name: 'Museum') }
    scope :price_policy, ->(price_policy) { where price_policy: price_policy }
  end
end
