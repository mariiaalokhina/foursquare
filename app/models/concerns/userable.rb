module Userable
  extend ActiveSupport::Concern

  included do
    before_validation :create_ref_user
  end

  def create_ref_user
    user = User.create if user.blank?
    self.user_id = user.id
  end
end
