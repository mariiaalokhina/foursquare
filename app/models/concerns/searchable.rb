module Searchable
  extend ActiveSupport::Concern

  included do
    include Elasticsearch::Model
    include Elasticsearch::Model::Callbacks
    include ActiveModel::Serializers::JSON

    index_name([Rails.env, base_class.to_s.pluralize.underscore].join('_'))
  end
end
