module Typenameble
  extend ActiveSupport::Concern

  included do
    after_create :default_type_name
  end

  def default_type_name
    self.type_name = self.class.name
    save
  end
end
