module Notificationable
  extend ActiveSupport::Concern

  included do
    after_create :create_notifications
    after_destroy :destroy_notification
  end

  def recipients
    admins = User.all.select { |user| user.is? 'admin' }
    admins.map(&:email)
  end

  def create_notifications
    recipients.each do |recipient|
      Notification.create(recipient: recipient, action: 'create', notifiable: id)
    end
  end

  def destroy_notification
    notifications = Notification.all.select { |n| n.notifiable.to_i == id }
    notifications.each(&:delete)
  end
end
