module Notificationvalidation
  extend ActiveSupport::Concern

  included do
    validates :recipient, length: { minimum: 2 }
    validates :action, length: { minimum: 2 }
    validates :notifiable, uniqueness: true
  end
end
