module Favoriteplacable
  extend ActiveSupport::Concern

  included do
    after_create :create_favoriteplace, :create_savedplace
  end

  def create_favoriteplace
    FavoritePlace.create(user: self)
  end

  def create_savedplace
    SavedPlace.create(user: self)
  end
end
