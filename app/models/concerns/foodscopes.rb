module Foodscopes
  extend ActiveSupport::Concern

  included do
    scope :bar, -> { where(type_name: 'Bar') }
    scope :cafe, -> { where(type_name: 'Cafe') }
    scope :club, -> { where(type_name: 'Club') }
    scope :coffee, -> { where(type_name: 'Coffee') }
    scope :fastfood, -> { where(type_name: 'Fastfood') }
    scope :price_policy, ->(price_policy) { where price_policy: price_policy }
  end
end
