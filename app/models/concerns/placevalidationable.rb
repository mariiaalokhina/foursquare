module Placevalidationable
  extend ActiveSupport::Concern

  included do
    validates :status, presence: true
    validates :address, length: { minimum: 5 }, presence: true
    validates :mark, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 5 }
  end
end
