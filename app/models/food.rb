class Food < ApplicationRecord
  include Notificationable
  include Placevalidationable
  include Foodscopes
  # include Searchable

  belongs_to :foursquare
  has_and_belongs_to_many :favorite_places
  has_and_belongs_to_many :saved_places

  enum price_policy: { cheap: 0, middle: 1, expensive: 2 }

  class << self
    def open_now(time)
      Food.select do |food|
        if food.open_hours.present?
          food.open_hours[week_day]['open'] < time && food.open_hours[week_day]['close'] > time
        end
      end
    end

    private

    def week_day
      Time.zone.today.strftime('%A').slice(0..2)
    end
  end
end
