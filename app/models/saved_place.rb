class SavedPlace < ApplicationRecord
  belongs_to :user
  has_and_belongs_to_many :places
  has_and_belongs_to_many :foods
end
