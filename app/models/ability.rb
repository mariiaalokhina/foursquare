class Ability
  include CanCan::Ability

  def initialize(user)
    @user = user || User.new
    @user.roles.each { |role| send(role) }

    can :read, :all if @user.roles.empty?
  end

  def admin
    can :manage, :all
  end
end
