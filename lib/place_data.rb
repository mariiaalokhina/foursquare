require 'geocoder'
module PlaceData
  class << self
    RANGE = (7..-1).freeze
    def change(data)
      data.each do |place|
        place[:address] = address(place[:address])
        place[:coordinates] = coordinates(place[:address])
        place[:status] = 'inactive'
        place[:foursquare_id] = Foursquare.find { |f| f.section == 'Place' }.id
        place[:mark] = rand(0.5...5.0).round(1)
        place[:price_policy] = [0, 1, 2].sample
        place[:open_hours] = {
            'Mon' => { 'open'=> open_time, 'close'=> close_time },
            'Tue' => { 'open'=> open_time, 'close'=> close_time },
            'Wed' => { 'open'=> open_time, 'close'=> close_time },
            'Thu' => { 'open'=> open_time, 'close'=> close_time },
            'Fri' => { 'open'=> open_time, 'close'=> close_time },
            'Sat' => { 'open'=> open_time, 'close'=> close_time },
            'Sun' => { 'open'=> open_time, 'close'=> close_time }
        }
      end
    end

    private

    def open_time
      %w[9:00 10:00 11:00].sample
    end

    def close_time
      %w[16:00 19:00 20:00 21.00].sample
    end

    def coordinates(address)
      unless address.nil?
        @address = address.values.join(', ')
        Geocoder.coordinates(@address)
      end
    end

    def address(data)
      unless data.nil? || data[RANGE].nil?
        address = data[RANGE].split(', ').map(&:strip)
        {
          street_address: address[1],
          city: address[0],
          house: address[2]
        }
      end
    end
  end
end
