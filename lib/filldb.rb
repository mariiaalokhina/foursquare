module FillDB
  def self.fill(fill, model)
    fill.each do |place|
      model.create!(place)
    end
  end
end
