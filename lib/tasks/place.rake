require 'foursquare_catalog'
require 'place_data.rb'
require 'filldb.rb'

namespace :db do
  desc 'Fill place db with  data'
  base_url = 'https://mykharkov.info/catalog/'
  options = { css_block: '.directory-section',
              css_name: '.h3',
              css_address: '.address',
              foursquare_id: 1 }
  task church: :environment do
    url = base_url + 'tserkvi-i-hramy'
    churches = FoursquareCatalog::Information.new('Church', url, options).parse
    fill = PlaceData.change(churches)
    FillDB.fill(fill, Place)
  end
  task monument: :environment do
    url = base_url + 'pamyatniki-monumenty-fontany'
    monuments = FoursquareCatalog::Information.new('Monument', url, options).parse
    fill = PlaceData.change(monuments)
    FillDB.fill(fill, Place)
  end
  task museum: :environment do
    url = base_url + 'muzei'
    museums = FoursquareCatalog::Information.new('Museum', url, options).parse
    fill = PlaceData.change(museums)
    FillDB.fill(fill, Place)
  end
end
