require 'foursquare_catalog'
require 'food_data.rb'
require 'filldb.rb'

namespace :db do
  desc 'Fill food db with  data'
  base_url = 'https://ru.foursquare.com/'
  options = { css_block: '.card',
              css_name: '.venueName',
              css_address: '.venueAddress',
              foursquare_id: 2 }

  task cafe: :environment do
    url = base_url + 'explore?cat=food&mode=url&near=%D0%A5%D0%B0%D1%80%D1%8C%D0%BA%D0%BE%D0%B2%2C%20%D0%A3%D0%BA%D1%80%D0%B0%D0%B8%D0%BD%D0%B0&nearGeoId=72057594038634419'
    cave = FoursquareCatalog::Information.new('Cafe', url, options).parse
    fill = FoodData.change(cave)
    FillDB.fill(fill, Food)
  end

  task bar: :environment do
    url = base_url + 'explore?cat=drinks&mode=url&near=%D0%A5%D0%B0%D1%80%D1%8C%D0%BA%D0%BE%D0%B2%2C%20%D0%A3%D0%BA%D1%80%D0%B0%D0%B8%D0%BD%D0%B0&nearGeoId=72057594038634419'
    bars = FoursquareCatalog::Information.new('Bar', url, options).parse
    fill = FoodData.change(bars)
    FillDB.fill(fill, Food)
  end

  task club: :environment do
    url = base_url + 'explore?mode=url&ne=50.049423%2C36.389637&q=%D0%A2%D0%B0%D0%BD%D1%86%D1%8B&sw=49.924704%2C36.159611'
    clubs = FoursquareCatalog::Information.new('Club', url, options).parse
    fill = FoodData.change(clubs)
    FillDB.fill(fill, Food)
  end

  task coffee: :environment do
    url = base_url + 'explore?cat=coffee&mode=url&near=%D0%A5%D0%B0%D1%80%D1%8C%D0%BA%D0%BE%D0%B2%2C%20%D0%A3%D0%BA%D1%80%D0%B0%D0%B8%D0%BD%D0%B0&nearGeoId=72057594038634419'
    coffees = FoursquareCatalog::Information.new('Coffee', url, options).parse
    fill = FoodData.change(coffees)
    FillDB.fill(fill, Food)
  end

  task fastfood: :environment do
    type_url = base_url + 'explore?mode=url&ne=50.131143%2C36.525078&q=%D0%9F%D0%B8%D1%86%D1%86%D0%B5%D1%80%D0%B8%D1%8F&sw=49.881805%2C36.065025'
    fastfoods = FoursquareCatalog::Information.new('Fastfood', type_url, options).parse
    fill = FoodData.change(fastfoods)
    FillDB.fill(fill, Food)
  end
end
