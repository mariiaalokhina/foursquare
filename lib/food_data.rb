require 'geocoder'
module FoodData
  class << self
    def change(data)
      data.each do |food|
        food[:name] = food[:name][3..-1]
        food[:address] = address(food[:address])
        food[:coordinates] = coordinates(food[:address])
        food[:status] = 'inactive'
        food[:foursquare_id] = Foursquare.find { |f| f.section == 'Food' }.id
        food[:mark] = rand(0.5...5.0).round(1)
        food[:price_policy] = [0, 1, 2].sample
        food[:open_hours] = {
            'Mon' => { 'open' => open_time, 'close' => close_time },
            'Tue' => { 'open' => open_time, 'close' => close_time },
            'Wed' => { 'open' => open_time, 'close' => close_time },
            'Thu' => { 'open' => open_time, 'close' => close_time },
            'Fri' => { 'open' => open_time, 'close' => close_time },
            'Sat' => { 'open' => open_time, 'close' => close_time },
            'Sun' => { 'open' => open_time, 'close' => close_time }
        }
      end
    end

    private

    def open_time
      %w[9:00 10:00 11:00].sample
    end

    def close_time
      %w[16:00 19:00 20:00 21.00].sample
    end

    def coordinates(address)
      unless address.nil?
        @address = address.values.join(', ')
        @address.gsub! 'просп.', 'проспект'
        @address.gsub! 'вул.', 'улица'
        Geocoder.coordinates(@address)
      end
    end

    def address(data)
      unless data.nil?
        @address = data.split(', ').map(&:strip)
        {
          city: @address[2],
          street_address: @address[0],
          house: @address[1].to_i
        }
      end
    end
  end
end
